require 'httparty'
require 'pp'
 
class Forecast
  def self.fetch
    url = 'http://api.openweathermap.org/data/2.5/forecast/daily?q=Ljubljana&mode=json&units=metric&cnt=16&appid=0f10da6e51637ee11b7c0c211d1aa662'
    response = HTTParty.get(url)
    response.parsed_response
  end
end