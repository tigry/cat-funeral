== README

Your cat just died and you are planning its funeral. It's a very crowded town with only one cemetary and you need to make sure there aren't any colisions with other dead cats.
You need a tool to reserve a time slot. A funeral takes 2 hours and can happen on every day of the week.

Your task is:

- Create a registration and login pages for funeral 'customers'
- Display a calendar view with taken and or free slots
- Display the weather for each day (hint: openweathermapp.org, the ceramony is located in Ljubljana)
- Create a reservation form, the user should be able to pick a slot
- Deploy your app to Heroku


Heroku url: https://limitless-ridge-14037.herokuapp.com/
