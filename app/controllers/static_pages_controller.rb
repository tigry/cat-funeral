class StaticPagesController < ApplicationController
  require 'forecast'
  
  def home
  end

  def help
  end
  
  def event
    @meetings = Meeting.all
    @forecast = Forecast.fetch
  end
  
end
