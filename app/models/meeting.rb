class Meeting < ActiveRecord::Base
    belongs_to :user
    validate :start_time_doesnt_overlap
  
    def start_time_doesnt_overlap
      if self.class.where("start_time >= ? AND start_time <= ? OR
        start_time <= ? AND start_time >= ?", start_time, start_time + 2.hours, 
        start_time, start_time - 2.hours).any?
        errors.add(:start_time, "overlaps with another funeral")
      end
    end
    
end
