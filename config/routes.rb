Rails.application.routes.draw do
  resources :meetings
  devise_for :users
  root                'static_pages#home'
  get   'help'   =>   'static_pages#help'
  get   'event'  =>   'static_pages#event'
  
  
  # Directing the user after login
  authenticated :user do
    root :to => 'static_pages#event', as: :authenticated_root
  end

end
