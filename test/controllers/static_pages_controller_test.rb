require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Home | Cat funeral"
  end

  test "should get help" do
    get :help
    assert_response :success
    assert_select "title", "Help | Cat funeral"
  end
  
    test "should get event" do
    get :event
    assert_response :success
    assert_select "title", "Event | Cat funeral"
  end
  
end
